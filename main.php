<?php
session_start();
require("prdModel.php");

//check whether the user has logged in or not
if ( ! isSet($_SESSION["loginProfile"] )) {
	//if not logged in, redirect page to loginUI.php
	header("Location: loginUI.php");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Basic HTML Examples</title>
</head>
<body>
<p>This is the MAIN page 
[<a href="logout.php">logout</a>]

</p>
<hr>
<?php
	echo "Hello ", $_SESSION["loginProfile"]["uName"],
	", Your ID is: ", $_SESSION["loginProfile"]["uID"],
	", Your Role is: ", $_SESSION["loginProfile"]["uRole"],"<HR>";
	$result=getPrdList();
?>
<a href="showOrders.php">List My Orders</a><hr>
	<table width="200" border="1">
  <tr>
    <td>id</td>
    <td>name</td>
    <td>price</td>
    
<?php
	if ($_SESSION["loginProfile"]["uRole"] == 9) {
		echo "<a href='admin.php'>Manage order</a><br/>";
		echo "<td>U</td>";
		echo "<td>R</td>";
		echo "<a href='addPrd.php'>Add Product</a>";
	} else {
		echo "<td>+</td>";
	}
?>
  </tr>
<?php
while (	$rs=mysqli_fetch_assoc($result)) {
	echo "<tr><td>" . $rs['prdID'] . "</td>";
	echo "<td>{$rs['name']}</td>";
	echo "<td>" , $rs['price'], "</td>";
	
	if ($_SESSION["loginProfile"]["uRole"] == 9) {
		echo "<td><a href=\"updatePrd.php?prdID=", $rs['prdID'], "\">Update</a></td>";
		echo "<td><a href=\"removePrd.php?prdID=", $rs['prdID'], "\">Remove</a></td>";	
	} else {
		echo "<td><a href='addToCart.php?prdID=" , $rs['prdID'] , "'>Add</a></td>";
	}
	echo "</tr>";
}
?>
</table>
<a href="showCartDetail.php">Checkout cart</a><hr>

</body>
</html>
