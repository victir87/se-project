<?php
session_start();
//check whether the user has logged in or not
if ( ! isSet($_SESSION["loginProfile"] )) {
	//if not logged in, redirect page to loginUI.php
	header("Location: loginUI.php");
}

require("orderModel.php");

$serno=$_GET['serno'];
if (removeFromCart($serno)) {
    echo "<h1>Remove item successfully!!!</h1>";
    echo "<a href=\"showCartDetail.php\">Click here to redirect to the cart page!!!</a>";
} else {
    echo "<h1>Something wrong when removing cart...</h1>";
}
?>
